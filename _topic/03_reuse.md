---
slug: reuse
title: Reuse & Reusability
---
# Reference materials and information on reuse and accessibility 

## What makes code re-useable?

* Wilson, et al. [Best Practices for Scientific Computing](https://journals.plos.org/plosbiology/article?id=10.1371/journal.pbio.1001745)
* [Ten Simple Rules for Reproducible Computational Research](http://dx.doi.org/10.1371/journal.pcbi.1003285)
* [FAIR data principles](https://www.force11.org/group/fairgroup/fairprinciples)
* A nice guide to [structuring your python repo](https://docs.python-guide.org/writing/structure/)

## Packages 

### Package managers 

Package managers automate the process of installing pre-built packages in a way that fits your system.  When a package manager is working ideally on your system, it knows which packages are installed already, and their versions, and knows how to follow the instructions for new packages so as to identify dependencies, generate a workflow, and execute the workflow to build and install the package and its requirements.  

The first rule of package managers is to pick one and use that all the time.  Don't install packages manually or use other managers, which can create a mess.  Unfortunately, it is not always possible to use only one package manager.      

* Wikipedia's list of [package management systems](https://en.wikipedia.org/wiki/List_of_software_package_management_systems)

### Package development 

Someone has to make those nice packages.  Maybe that's you.  Ideally your package is amenable to automated installation because it follows the conventions expected by package installers.  Package generation tools exist for R, Python and other languages.  

Package generation tools 
* [devtools](https://cran.r-project.org/web/packages/devtools/index.html) for R packages
* [quickie intro to devtools](https://www.rstudio.com/products/rpackages/devtools/)
* introduction to using Python's [setuptools](https://packaging.python.org/tutorials/packaging-projects/)

There are also best practices for structuring your repositories.  
* [General discussion of best practices for R Packages](http://r-pkgs.had.co.nz/)
* [Python Project Template Guide](http://learnpythonthehardway.org/book/ex46.html) 
* A nice guide to [structuring your python repo](https://docs.python-guide.org/writing/structure/)
see the workspace reference for hadley wickham guide to R packages.  
* [...or search for templates online](https://www.google.com/search?q=python%20package%20template)

These are best practices for data repositories, but I find them useful for code as well
* Dryad's instructions to [Name files and directories in a consistent and descriptive manner](https://datadryad.org/pages/reusabilityBestPractices#filenames)
* Dryad's instructions to [Organize files in a logical schemaf](https://datadryad.org/pages/reusabilityBestPractices#organize)

## Standing up an accessible service 

Sometimes the best way to make your work accessible to others is to provide the code. In other cases, it is better to provide functionality directly through a web application, a web service, or other means.  

* Python's [web2py](http://www.web2py.com/)
* R's [rshiny](https://www.rstudio.com/products/shiny-2/)

## cross-platform compilation

* [Brian’s 10 Rules for how to write cross-platform code](https://www.backblaze.com/blog/10-rules-for-how-to-write-cross-platform-code/)

## Licensing

* [choosing a license](https://www.cio.com/article/2382115/open-source-tools/how-to-choose-the-best-license-for-your-open-source-software-project.html)