---
slug: io
title: Input & Output (IO)
---

# Preliminaries

## Setup 

The instructor is going to work primarily in R, but you can follow along in Python.  Everyone has Python installed already.  To use R, install R and (for best results) RStudio ([how to install](https://www.datacamp.com/community/tutorials/installing-R-windows-mac-ubuntu)).
* Point your browser to SEAMS web site --> Practicals --> IO ([this page](https://seams-workshop.gitlab.io/practical/io/)) 
* Open a text editor. You will need to inspect files and edit some commands.
* Create a directory for this session called "io_practical"
* Launch your Python or R interpreter with **io_practical** as the working directory.  Typically someone forgets to do this and then the commands don't work. 
   * in RStudio, use Session -> Set Working Directory
   * in R, use `setwd("./path/to/your/io_practical")`
   * in Python, `import os` then `os.chdir("./path/to/your/io_practical")`
* Open a terminal and cd to your **io_practical** directory. 
* Download [this crystal structure for Cytochrome C](https://www.rcsb.org/structure/1hrc) into **io_practical**

## Instructions

We are going to do some exercises that involve reading stuff in, and writing it out again. 

For some exercises, you will work alone or in pairs on a challenge.  For other exercises, you will follow the instructor step by step.  

The follow-along parts are written in R with some UN\*X shell and SQL (SQLite). Python tips are provided.  To follow along, you will have to copy and paste lines of code into your R (or Python) and SQLite interpreter. 

For best results with R, **use RStudio**, which provides command-completion and syntax highlighting.  If you download the source for this file ([here](https://gitlab.com/SEAMS-Workshop/seams-workshop.gitlab.io/blob/master/_practical/04_io.md))  and change the extention to ".Rmd" (put it in your **io_practical** directory), RStudio will recognize the code chunks and you can run the R bits directly.

If you are not familiar with R or Python, pair up with someone who is.  

# Review 

Types of file formats you might encounter
* generic formats, e.g.,
   * CSV (comma-separated values) for tabular data
   * JSON for hierarchically structured data
* language-specific binaries or serializations
   * SQL: dump (text file with full SQL instructions for recreating a database)
   * Python: pickles
   * R binaries
* domain-specific formats, e.g., 
   * FASTA, GenBank (DNA, RNA, protein sequences)
   * PDB (protein structures)
   * SPC (spectroscopy data)
   * Flexible Image Transport System (FITS) (astronomy)
* custom formats.  Try not to create your own format unless you have to.  

# Exercises 

## Reading from a domain-specific format (participants follow instructor)

We will begin with a domain-specific format and select some parts of it for output.  

### The source file 

The CytochromeC structure you downloaded is in **PDB** (Protein Data Bank) format. PDB is a line-based format with fixed column widths. The heart of the file is the block of "ATOM" lines that define the 3D coordinates of atoms.  This is the definition for atom lines (see the [ATOM section of the format description](http://www.wwpdb.org/documentation/file-format-content/format33/sect9.html#ATOM)): 

|from|to|type|label|explanation|
|----|--|----|-----|-------------|
|1|6|Record name|ATOM||
|7|11|Integer|serial|Atom serial number.|
|13|16|Atom|name|Atom name.|
|17|17|Character|altLoc|Alternate location indicator.|
|18|20|Residue name|resName|Residue name.|
|22|22|Character|chainID|Chain identifier.|
|23|26|Integer|resSeq|Residue sequence number.|
|27|27|AChar|iCode|Code for insertion of residues.|
|31|38|Real(8.3)|x|Orthogonal coordinates for X in Angstroms.|
|39|46|Real(8.3)|y|Orthogonal coordinates for Y in Angstroms.|
|47|54|Real(8.3)|z|Orthogonal coordinates for Z in Angstroms.|
|55|60|Real(6.2)|occupancy|Occupancy.|
|61|66|Real(6.2)|tempFactor|Temperature factor.|
|77|78|LString(2)|element|Element symbol, right-justified.|
|79|80|LString(2)|charge|Charge|

Here is what you will see if you inspect this file using a text editor, or using UN\*X commands:  
```{shell}
wc *.pdb
    1365   14294  110565 1hrc.pdb
head *.pdb
   HEADER    ELECTRON TRANSPORT(CYTOCHROME)          16-AUG-94   1HRC              
   TITLE     HIGH-RESOLUTION THREE-DIMENSIONAL STRUCTURE OF HORSE HEART            
   TITLE    2 CYTOCHROME C                                                         
   COMPND    MOL_ID: 1;                                                            
   COMPND   2 MOLECULE: CYTOCHROME C;                                              
   COMPND   3 CHAIN: A;                                                            
   COMPND   4 ENGINEERED: YES                                                      
   SOURCE    MOL_ID: 1;                                                            
   SOURCE   2 ORGANISM_SCIENTIFIC: EQUUS CABALLUS;                                 
   SOURCE   3 ORGANISM_COMMON: HORSE; 
grep "^ATOM.* CA " 1hrc.pdb | head 
   ATOM      5  CA  GLY A   1      53.439  13.215  -4.519  1.00 48.48           C  
   ATOM      9  CA  ASP A   2      55.283  15.800  -6.535  1.00 41.19           C  
   ATOM     17  CA  VAL A   3      56.907  17.792  -3.754  1.00 37.97           C  
   ATOM     24  CA  GLU A   4      58.056  20.536  -6.199  1.00 37.88           C  
   ATOM     33  CA  LYS A   5      54.710  20.703  -7.977  1.00 35.71           C  
   ATOM     42  CA  GLY A   6      53.314  20.904  -4.447  1.00 33.80           C  
   ATOM     46  CA  LYS A   7      55.544  23.886  -3.523  1.00 28.65           C  
   ATOM     55  CA  LYS A   8      54.256  25.965  -6.425  1.00 26.23           C  
   ATOM     64  CA  ILE A   9      50.588  25.410  -5.398  1.00 22.94           C  
   ATOM     72  CA  PHE A  10      51.601  26.428  -1.894  1.00 27.68           C  
```

The X, Y and Z coordinates for each atom are the first 3 floating-point numbers you see on each ATOM line.  Let's look at all the atoms for residues 12 and 29, taking advantage of the fact that the residue number ("resSeq" field) is always in positions 23 to 26.  Using `egrep` makes this command simple. 
```{shell}
egrep "^ATOM.{18}  (12|29) " 1hrc.pdb
   ATOM     89  N   GLN A  12      51.501  29.976  -4.408  1.00 31.40           N  
   ATOM     90  CA  GLN A  12      50.375  30.514  -5.151  1.00 31.67           C  
   ATOM     91  C   GLN A  12      49.054  30.576  -4.444  1.00 31.13           C  
   ATOM     92  O   GLN A  12      48.317  31.541  -4.708  1.00 29.51           O  
   ATOM     93  CB  GLN A  12      50.264  29.767  -6.508  1.00 34.57           C  
   ATOM     94  CG  GLN A  12      51.513  30.093  -7.335  1.00 35.91           C  
   ATOM     95  CD  GLN A  12      51.464  29.667  -8.768  1.00 38.95           C  
   ATOM     96  OE1 GLN A  12      50.441  29.308  -9.367  1.00 38.07           O  
   ATOM     97  NE2 GLN A  12      52.679  29.705  -9.350  1.00 38.78           N  
   ATOM    218  N   GLY A  29      46.500  33.426   8.856  1.00 20.25           N  
   ATOM    219  CA  GLY A  29      46.336  32.022   9.303  1.00 18.56           C  
   ATOM    220  C   GLY A  29      47.615  31.639  10.031  1.00 18.21           C  
   ATOM    221  O   GLY A  29      48.707  32.207   9.894  1.00 20.80           O

```

Does everyone have the source file?  Has everyone seen the atom lines?  We are going to extract the CA (alpha carbon) lines for residues 12 and 29. 

### installing a package to read the file (participants follow instructor in either R or Python)

Typically, for a popular domain-specific format, there are pre-existing libraries.  PDB format has been around for several decades, so there are many libraries available.  For R, I'm going to use a small package that only deals with PDB files, [RPdb](https://rdrr.io/cran/Rpdb/). 

```{r}
install.packages("Rpdb")  # install the package once (no need to repeat)
library("Rpdb")           # load the library each time you want to use it

cyto_c <- read.pdb("1hrc.pdb")
```

If you loaded the library but the command does not work, the most likely reason is that you need to use `setwd("path/to/io_practical")` to set the working directory so you can access the PDB file.  

For Python, let's just go straight to installing BioPython from the shell
```{shell}
python3 -m pip install biopython
```
Now, in Python, read in the file to create an object. 
```{python}
from Bio.PDB import *
parser = PDBParser()
cyto_c = parser.get_structure('CytoC', '1hrc.pdb')
```
   
## Exercise: get some data and dump it in native object format (students work alone or in pairs)

Estimated time for this is 20 minutes.  You have the object in memory already. Your task now is to figure out how to select records for 2 specific atoms and save this information to a file that can be re-read by yourself or another user.  

1. find the package documentation or an online how-to for the package you used.  Figure out how to select specific residues or atoms. 
1. select the alpha-Carbon atoms ("CA" in column 3, the "Atom name" column) for residues 12 and 29 (lines 407 and 537 of the original file).  If you get stuck on this part, just pick any 2 atoms or create an object manually (copy and paste from the ATOM lines above). 
1. save the selected atoms as an object in the native data format.  In R this is RDS or RData.  In Python, it's pickle.
1. delete the object from your environment, then re-load it from the file. 

Instructor: answer for R is hidden in the comments to this file.  

<!-- here is the answer in R 










We are going to grab just 2 lines of data to make a table, the CA (alpha carbon) atoms of residues 12 and 29.  

```{r}
my_cas <- cyto_c$atoms[cyto_c$atoms$elename == 'CA',]          # first, get all the CA rows
my_data <- data.frame(my_cas[my_cas$resid %in% c(12, 29), ])   # next, get the 2 rows we want
my_data
```

Now we will use R's native binary formats to save it.    Save one object like this: 

```{r}
# save a single object to file
saveRDS(my_data, file = "my_data.rds")

# restore the object
new_my_data <- readRDS(file = "my_data.rds")
```

If you want to save multiple objects, you can do that with a different method.  The objects will be stored with their names intact, and they will have those same names when you reload them. 

```{r}
# save multiple named objects
other_data <- data.frame(my_cas[my_cas$resid %in% c(13, 30), ])
save(my_data, other_data, file = "all_data.RData")

# delete the objects
rm(my_data, other_data)
my_data

# load the objects into memory with their original names
load("all_data.RData")
my_data
```
--> 

How do you select the data in Python?  Share it in the Slack channel. 

This is how the data can be exported and imported in Python, assuming that you have selected the data and stored it in my_data.  

```{python}
import pickle
pickle.dump(my_data, open("my_data.p", "wb"))

my_imported_data <- pickle.load(open("my_data.p", "rb"))
```

## Writing out a CSV file (follow along)

Our results in R are already tabular. The functions to read and write CSV are included in the "utils" package in R, which is a base package included in every R distribution.  

```{r}
# show what it looks like
my_data
write.csv(my_data, "my_data.csv", row.names = FALSE)

# or you could do tab-separared values
# write.csv(my_data, "my_data.tsv", row.names = F, sep = "\t")
```

This is what our file looks like.  Notice that the Rpdb package gives names to the columns that are not the same as the labels used in the PDB format docs, and it seems to fuse the last 2 (empty) columns into 1: 

```
"recname","eleid","elename","alt","resname","chainid","resid","insert","x1","x2","x3","occ","temp","segid"
"ATOM",90,"CA","","GLN","A",12,"",50.375,30.514,-5.151,1,31.67,""
"ATOM",219,"CA","","GLY","A",29,"",46.336,32.022,9.303,1,18.56,""
```

Who knows how to do this in Python?  Share it in the Slack channel.  

## Reading from a CSV file (follow along)

```{r}
new_my_data <- read.csv("my_data.csv", header = T, sep = ",", stringsAsFactors = F)
new_my_data == my_data
```

Who knows how to do this in Python?  Share it in the Slack channel.  

## Reading and writing from JSON (students work alone or in pairs)

Estimated time for this is 20 minutes. If time is running short, choose between this and the next exercise.  

Exercise
* Find and install a package for reading and writing JSON.  If you aren't using R or Python, you will have to figure out how to input the data (e.g., copy the CSV text above into a file, and read that).  
* Output our 2 lines of atom data in JSON format.  
* Input the file again

## Creating and using a Database (follow-along)

Skip to wrap-up if there is not time for this. 

Do this in pairs so that, if one person gets stuck during setup, you can rely on the other person. 

### setup and test sqlite

Put these two things in the directory you made for this lesson: 
1. A [pre-compiled binary of SQLite3](https://www.sqlite.org/download.html). 
1. The [survey.db](https://swcarpentry.github.io/sql-novice-survey/files/survey.db) (click to download) demo database from [Software Carpentry's SQL lesson](https://swcarpentry.github.io/sql-novice-survey/).  We are going to use this for testing. 

Now, launch `sqlite3` from the shell, using the database just mentioned.  You have to paste this command into your UN\*X shell. 
```{shell}
sqlite3 survey.db
```

Now you should see the prompt for the SQLite interpreter, which is "sqlite>". We will do a few quick things, then quit using the `.quit` command. The `.tables` command lists the tables in this db.  In SQL, the double-dash indicates a comment. Run these commands in the SQLite interpreter ("sqlite>" prompt):
```{sql}
.mode column     -- show me nice columns (dot commands are not SQL and do not end with semi-colon)
.header on       -- with column labels
.tables          -- list the tables in this db
.schema Person   -- show me how the Person table is defined
SELECT family, personal FROM Person;     -- SQL commands end with semi-colon
.quit
```

This is the expected result: 

|family|personal|
|----------|----------|
|Dyer|William|
|Pabodie|Frank|
|Lake|Anderson|
|Roerich|Valentina|
|Danforth|Frank|

### Make an atoms database  

Now, we are going to make a table for our 2 alpha carbons.  This table really belongs in a new db instead of survey.db, so be sure that you exit survey.db using `.quit`, then create a new sqlite db from the UN\*X shell: 

```{shell}
sqlite3 practice.db
```

### Import the easy stupid way

Importing our data is super easy, barely an inconvenience. From the [SQLite tutorial on CSV import](http://www.sqlitetutorial.net/sqlite-import-csv/) the `.import` command just takes two arguments: the file and the table in which to insert the data. If the table does not exist already, it will be created. Run these commands in the SQLite interpreter ("sqlite>" prompt):

```{sql}
.header on
.mode csv                       -- notice that we have to invoke "csv" mode here
.import "my_data.csv" Atoms
SELECT * FROM Atoms;
```

### Importing data more safely

The problem is that the table created with this command is not very smart.  It casts everything as a TEXT field (see the results of the `.schema` command below).  So, we are going to `DROP` (delete) this table from the db.  Run these commands in the SQLite interpreter ("sqlite>" prompt):

```{sql}
.mode column
.schema Atoms
DROP TABLE Atoms;
```

We want to define a table.  We only get 3 data types (see [list](http://www.sqlitetutorial.net/sqlite-data-types/)).  Which one do we use for the x, y and z coordinates?  Edit this command and run it in the SQLite interpreter ("sqlite>" prompt):
```{sql}
CREATE TABLE Atoms(
   ATOM TEXT,
   serial INTEGER,
   name TEXT,
   altLoc TEXT,
   resName TEXT,
   chainID TEXT,
   resSeq INTEGER,
   iCode TEXT,
   x ,               -- What do I need to put in here for type?
   y ,               -- What do I need to put in here for type?
   z ,               -- What do I need to put in here for type?
   occupancy REAL,
   tempFactor REAL,
   element TEXT,
   charge TEXT
);
.schema Atoms
```

The `.schema` command should echo back to us the structure we just created.  Now we can import our data.

**Importantly**, when we import into a table that is already created, the `.import` command assumes that the file does not have a header. So, we have to make a version of the CSV file without the header, using a text editor or a UN\*X command like this: 

```{shell}
grep -v "resid" my_data.csv > my_data_no_header.csv
```

Now we can import the data.  Run these commands in the SQLite interpreter ("sqlite>" prompt):
```{sql}
.header on
.mode csv
.import "my_data_no_header.csv" Atoms
SELECT * FROM Atoms;
```

The warning about 14 vs. 15 fields happens because of the Rpdb package, which mis-defines the ATOM lines.  Notice also that the Rpdb package does not use the column labels used in the PDB format definition.  Sometimes packages have mistakes.  

### Exporting (dumping) from an SQL database

Your database can be reconstructed completely using only SQL commands, which means that it can be exported as a text file.  An export of your database as a list of SQL instructions is called a **dump**, implemented in sqlite with the `.dump` command.  Run these commands in the SQLite interpreter ("sqlite>" prompt):

```{sql}
.output "practice.sql"      -- designate the name of the output file
.dump
```

The great thing about this format is that it is human-readable text. This is the whole thing: 

```{sql}
PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE Atoms(
   ATOM TEXT,
   serial INTEGER,
   name TEXT,
   altLoc TEXT,
   resName TEXT,
   chainID TEXT,
   resSeq INTEGER,
   iCode TEXT,
   x REAL,
   y REAL,
   z REAL,
   occupancy REAL,
   tempFactor REAL,
   element REAL,
   charge REAL
);
INSERT INTO "Atoms" VALUES('ATOM',90,'CA','','GLN','A',12,'',50.375,30.514,-5.151,1.0,31.67,'',NULL);
INSERT INTO "Atoms" VALUES('ATOM',219,'CA','','GLY','A',29,'',46.336,32.022,9.303,1.0,18.56,'',NULL);
COMMIT;
```

### Importing from an SQL dump

Every implementation of SQL has a slightly different set of internal commands.  In SQLite, these are the commands that begin with a dot.  They are different in other languages.  In SQLite, the command to import or load a dump is `.read`. 

```{sql}
.read "practice.sql"
```

This means you can send your entire database (with multiple tables, linked together) to someone as a text file, and they can recreate it locally in one easy step.  

## SQL implementations differ 

Note that SQL implementations-- SQLite, MySQL, PostgresQL, and others-- differ from each other. They implement SQL commands like SELECT in the same way, but they also implement other commands that are not the same.  For instance, if you use MySQL instead of SQLite, you would
* use `show tables;` instead of `.tables`
* use `describe Atoms;` instead of `.schema Atoms`
* export with `mysqldump`, a separate tool, instead of executing `.dump` inside the SQL interpreter

In the examples used in this lesson, the SQL keywords are in UPPERCASE, and the SQLite-specific commands have lower-case names that begin with a dot.  SQL keywords are not case-sensitive, i.e., `select * from Atoms;` is the same as `SELECT * FROM Atoms;`, but it is conventional to put them in uppercase for clarity.  

# Wrap-up

If we had more time, the next topic on the list would be programmatic access to databases using libraries variously called **connectors**, **drivers** or **bindings**.  Connectors allows scripts or programs to access a database without issuing SQL commands interactively.  For instance, there are R and Python connectors for SQLite; there are C, C++, Python, and various other drivers for MySQL ([list of MySQL drivers](https://www.mysql.com/products/connector/)).

And the next topic after that might be going directly between SQL data and JSON, because there are libraries for that, too, e.g., there is a [JSON1](https://www.sqlite.org/json1.html) extension for SQLite.  

Final questions for discussion.  Ask them all at once. 
* What are the most challenging aspects of IO?   
* What is the most significant or useful thing that you learned today?  
* How are you going to think differently the next time you are faced with an IO task?  

<!-- @Perceval adds Python examples below for going from JSON to database via connector 










-->
<!--
# Generate JSON From SQL Using Python

```
Steps to take during the practical
1.Install python3
2.Import Database connector based on the database used i.e  pyodbc for Sql server, psycopg2 for PostgresSQL 
3.Create a table or tables to query in your SQL database and write and test your query.   
In this example, I have a table called Students that has a few fields for each student. The query is simple:
```
    SELECT ID, FirstName, LastName, Street, City, ST, Zip
    FROM Students
```

4. Here’s an example script that generates two JSON files from that query.
One file contains JSON row arrays, and the other JSON key-value objects. Below, we’ll walk through it step-by-step.
```
    import pyodbc
    import json
    import collections
    connstr = 'DRIVER={SQL Server};SERVER=ServerName;DATABASE=Test;'
    conn = pyodbc.connect(connstr)
    cursor = conn.cursor()
    cursor.execute("""
                SELECT ID, FirstName, LastName, Street, City, ST, Zip
                FROM Students
                """)
    rows = cursor.fetchall()
    # Convert query to row arrays
    rowarray_list = []
    for row in rows:
        t = (row.ID, row.FirstName, row.LastName, row.Street, 
             row.City, row.ST, row.Zip)
        rowarray_list.append(t)
    j = json.dumps(rowarray_list)
    rowarrays_file = 'student_rowarrays.js'
    f = open(rowarrays_file,'w')
    print >> f, j
    # Convert query to objects of key-value pairs
    objects_list = []
    for row in rows:
        d = collections.OrderedDict()
        d['id'] = row.ID
        d['FirstName'] = row.FirstName
        d['LastName'] = row.LastName
        d['Street'] = row.Street
        d['City'] = row.City
        d['ST'] = row.ST
        d['Zip'] = row.Zip
        objects_list.append(d)
    j = json.dumps(objects_list)
    objects_file = 'student_objects.js'
    f = open(objects_file,'w')
    print >> f, j
        
    conn.close()


* Set a connection string to the Server      
* Set a connection string to the server. Then, we use pyodbc to open that connection and execute the query:    

``` 
    connstr = 'DRIVER={SQL Server};SERVER=ServerName;DATABASE=Test;'
    conn = pyodbc.connect(connstr)
    cursor = conn.cursor()
    cursor.execute("""
                SELECT ID, FirstName, LastName, Street, City, ST, Zip
                FROM Students
                """)
    rows = cursor.fetchall()

```
* The script above loads the query results into a list object called rows, which we can iterate through to do any number of things.  
In this case, we’ll build JSON.  

* At the top of the file, the script imports Python’s json module, which translates Python objects to JSON and vice-versa. Python lists and tuples become arrays while dictionaries become objects with key-value pairs.  

* In the first example, the script builds a list of tuples, with each row in the database becoming one tuple. Then, the json module’s “dumps” method is used to serialize the list of tuples to JSON, and we write to a file:  
```
    rowarray_list = []
    for row in rows:
        t = (row.ID, row.FirstName, row.LastName, row.Street, 
             row.City, row.ST, row.Zip)
        rowarray_list.append(t)
    j = json.dumps(rowarray_list)
    rowarrays_file = 'student_rowarrays.js'
    f = open(rowarrays_file,'w')
    print >> f, j
```

* The JSON result looks like this, with each tuple in a JSON array:

```
    [
        [
            1,
            "Samantha",
            "Baker",
            "9 Main St.",
            "Hyde Park",
            "NY",
            "12538"
        ],
        [
            2,
            "Mark",
            "Salomon",
            "12 Destination Blvd.",
            "Highland",
            "NY",
            "12528"
        ]
    ]
```

 * The script next builds a list of dictionaries, with each row in the database becoming one dictionary and each field in the row a key-value pair:
```
    objects_list = []
    for row in rows:
        d = collections.OrderedDict()
        d['id'] = row.ID
        d['FirstName'] = row.FirstName
        d['LastName'] = row.LastName
        d['Street'] = row.Street
        d['City'] = row.City
        d['ST'] = row.ST
        d['Zip'] = row.Zip
        objects_list.append(d)
    j = json.dumps(objects_list)
    objects_file = 'student_objects.js'
    f = open(objects_file,'w')
    print >> f, j
```
* Using Python’s OrderedDict() object from its collections class in place of a regular dictionary. While this is not necessary, I like to use it so I can force the order of dictionary keys to make the JSON more readable for fact-checking. Be sure to import collections at the beginning of your script.
```
    [
        {
            "id": 1,
            "FirstName": "Samantha",
            "LastName": "Baker",
            "Street": "9 Main St.",
            "City": "Hyde Park",
            "ST": "NY",
            "Zip": "12538"
        },
        {
            "id": 2,
            "FirstName": "Mark",
            "LastName": "Salomon",
            "Street": "12 Destination Blvd.",
            "City": "Highland",
            "ST": "NY",
            "Zip": "12528"
        }
    ]
```

-->
